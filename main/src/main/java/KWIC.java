package main.src.main.java;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class KWIC {


    public ArrayList getIgnoreList(ArrayList<String>TestList) {
        int i=0;
        ArrayList<String> ignore = new ArrayList<>();
        while(true) {
            String temp = TestList.get(i);
            if (temp.equals("::")) {
                //System.out.println("skip 1");
                break;
            }
            ignore.add(temp);
            i++;
        }
        return ignore;
    }
    public ArrayList<String> getTitleList(ArrayList<String>TestList) {
        int i=0;
        int index =0;
        ArrayList<String> title = new ArrayList<>();
        int n= TestList.size();
        while(i<n) {
            String temp = TestList.get(i);
            if (temp.equals("::")) {
                index =1;
                i=i+1;
                continue;
            }

            if(index ==1){
                title.add(temp);
            }
            i++;
        }
        return title;
    }

    public ArrayList<String> Split(String temp){
        String[] s = temp.split(" ");
        ArrayList<String> str = new ArrayList<>();
        int n=s.length;
        int i=0;
        while(i<n){
            str.add(s[i]);
            //System.out.println(str.get(i));
            i++;
        }
        return str;

    }

    public ArrayList<String> Generalizelowercase(String temp){
        ArrayList<String> str = Split(temp);
        ArrayList<String> s = new ArrayList<>();
        int n= str.size();
        int i=0;
        while(i<n){
            String string=str.get(i);
            string = string.toLowerCase();
            s.add(string);
            // System.out.println(string);
            i++;
        }
        return s;
    }

    public ArrayList<String> GetKeywordList(ArrayList<String> TestList){
        ArrayList<String> keyword = new ArrayList<>();
        ArrayList<String> ignore = getIgnoreList(TestList);
        ArrayList<String> title = getTitleList(TestList);
        int m= title.size();
        int i=0;
        while(i<m){
            int j=0;
            ArrayList<String> str = Generalizelowercase(title.get(i));
            int n= str.size();
            while(j<n){
                if((!ignore.contains(str.get(j)))&&(!keyword.contains(str.get(j)))){
                    keyword.add(str.get(j));
                }
                j++;}
            i++;
        }
        Collections.sort(keyword);


        return keyword;
    }
    public String toString(ArrayList<String> s){
        String str ="";
        int n = s.size();
        for(int i=0;i<n;i++){
            if(i ==n-1){
                str = str+s.get(i);
            }
            else{
                str = str+s.get(i)+" ";}

        }
        return str;
    }
    public ArrayList<String> Output(ArrayList<String> TestList){
        ArrayList<String> title =getTitleList(TestList);
        ArrayList<String> keyword =GetKeywordList(TestList);
        ArrayList<String> Output= new ArrayList<>();
        String s= "";
        int n = title.size();
        int l = keyword.size();
        for(int i=0;i<l;i++){
            for(int j=0;j<n;j++){
                ArrayList<String> line = Generalizelowercase(title.get(j));

                int m = line.size();
                int k=0;
                while(k<m){
                    if((line.get(k).equals(keyword.get(i)))){
                        line.set(k, line.get(k).toUpperCase());
                        Output.add(toString(line));
                        //System.out.println("1")

                    }
                    line = Generalizelowercase(title.get(j));
                    k++;
                }

            }

        }
        return Output;
    }
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<String> TestList=new ArrayList<>();
        while(true){
            String temp = scanner.nextLine();
            TestList.add(temp);
            if(temp.equals("")){
                break;
            }

        }
        ArrayList<String> keywordlist = new KWIC().Output(TestList);
        int n = keywordlist.size();
        for(int i=0;i<n;i++){
            System.out.println(keywordlist.get(i));
        }
    }






}

