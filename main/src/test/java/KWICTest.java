package main.src.test.java;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

import main.src.main.java.KWIC;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.*;
public class KWICTest {
    public ArrayList<String> TestList() {
        ArrayList<String> TestList = new ArrayList<>();
        TestList.add("is");
        TestList.add("the");
        TestList.add("of");
        TestList.add("and");
        TestList.add("as");
        TestList.add("a");
        TestList.add("but");
        TestList.add("::");
        TestList.add("Descent of Man");
        TestList.add("The Ascent of Man");
        TestList.add("The Old Man and The Sea");
        TestList.add("A Portrait of The Artist As a Young Man");
        TestList.add("A Man is a Man but Bubblesort IS A DOG");

        return TestList;
    }


    @Test
    void testIgnoreList(){
      ArrayList<String> expected = new ArrayList<String>();
      expected.add("is");
      expected.add("the");
      expected.add("of");
      expected.add("and");
      expected.add("as");
      expected.add("a");
      expected.add("but");
      assertEquals(new KWIC().getIgnoreList(TestList()),expected);
    }
    @Test

    public void testTitleList(){
        ArrayList<String> expected = new ArrayList<String>();
        expected.add("Descent of Man");
        expected.add("The Ascent of Man");
        expected.add("The Old Man and The Sea");
        expected.add("A Portrait of The Artist As a Young Man");
        expected.add("A Man is a Man but Bubblesort IS A DOG");
        assertEquals(new KWIC().getTitleList(TestList()),expected);
    }
    @Test
    public void testsplitfunction(){
        ArrayList<String> expected = new ArrayList<>();
        expected. add("Descent");
        expected. add("of");
        expected.add("Man");
        assertEquals(new KWIC().Split(TestList().get(8)),expected);
    }
    @Test
    public void testlowercase(){
        ArrayList<String> expected = new ArrayList<>();
        expected.add("descent");
        expected.add("of");
        expected.add("man");
        assertEquals(new KWIC().Generalizelowercase(TestList().get(8)),expected);
    }

    @Test
    //@Disabled("Remove this line to add this test.")
    public void testKeyword(){
        ArrayList<String> expected = new ArrayList<>();
        expected.add("artist");
        expected.add("ascent");
        expected.add("bubblesort");
        expected.add("descent");
        expected.add("dog");
        expected.add("man");
        expected.add("old");
        expected.add("portrait");
        expected.add("sea");
        expected.add("young");
        assertEquals(new KWIC().GetKeywordList(TestList()),expected);
    }

    @Test
//  @Disabled("Remove this line to add this test.")
    public void testResult(){
        ArrayList<String> expected = new ArrayList<String>();
        expected.add("a portrait of the ARTIST as a young man");
        expected.add("the ASCENT of man");
        expected.add("a man is a man but BUBBLESORT is a dog");
        expected.add("DESCENT of man");
        expected.add("a man is a man but bubblesort is a DOG");
        expected.add("descent of MAN");
        expected.add("the ascent of MAN");
        expected.add("the old MAN and the sea");
        expected.add("a portrait of the artist as a young MAN");
        expected.add("a MAN is a man but bubblesort is a dog");
        expected.add("a man is a MAN but bubblesort is a dog");
        expected.add("the OLD man and the sea");
        expected.add("a PORTRAIT of the artist as a young man");
        expected.add("the old man and the SEA");
        expected.add("a portrait of the artist as a YOUNG man");
        assertEquals(new KWIC().Output(TestList()),expected);

    }









}
